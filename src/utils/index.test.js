import { stopEvent } from './index'

it('invokes stopPropagation on passed event', () => {
  const e = {
    stopPropagation: jest.fn()
  }

  stopEvent(e)

  expect(e.stopPropagation).toHaveBeenCalled()
})
