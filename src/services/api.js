import superagent from 'superagent'

let uploadReq = null;

const API = {
  url: 'localhost',//process.env.REACT_APP_API_HOST,
  loginUrl: 'localhost',//process.env.REACT_APP_API_HOST,

  // Auth

  checkSession() {
    const url = `${this.loginUrl}/auth/console`;

    const params = {
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    };

    return (
      fetch(url, params)
        .then(response => {
          if (response.status !== 200) {
            throw new Error('Учетная запись не активна')
          }
          return response
        })
    )
  },
};

export default API