import API from '../services/api'

export function startEnvInitialize() {
  return (dispatch, getState) => {
    return (
      API.checkSession()
         .then(response => response.json())
         .then(profile => dispatch(setEnvUserProfile(profile)))
         .catch(() => dispatch(setEnvUserProfile(null)))
    )
  }
}

export function setEnvUserProfile(profile) {
  return {
    type: 'SET_ENV_USER_PROFILE',
    profile
  }
}

export function startEnvUserLogin() {
  return {
    type: 'START_ENV_USER_LOGIN'
  }
}

export function stopEnvUserLogin() {
  return {
    type: 'STOP_ENV_USER_LOGIN'
  }
}

export function setEnvUserError(hasError) {
  return {
    type: 'SET_ENV_USER_ERROR',
    hasError
  }
}

export function sendEnvUserCredentials(username, password) {
  return (dispatch, getState) => {
    dispatch(startEnvUserLogin())
    return (
      API.sendCredentials({username, password})
         .then(response => response.json())
         .then(profile => dispatch(setEnvUserProfile(profile)))
         .catch(() => dispatch(setEnvUserError(true)))
    )
  }
}

export function deleteEnvUserSession() {
  return (dispatch, getState) => {
    return (
      API.deleteSession()
        .then(() => dispatch(setEnvUserError(false)))
        .then(() => dispatch(setEnvUserProfile(null)))
    )
  }
}

export function layoutShowRightPanel() {
  return {
    type: 'LAYOUT_SHOW_RIGHT_PANEL'
  }
}

export function layoutHideRightPanel() {
  return {
    type: 'LAYOUT_HIDE_RIGHT_PANEL'
  }
}

export function layoutToggleVersionInfo() {
  return {
    type: 'LAYOUT_TOGGLE_VERSION_INFO'
  }
}

export * from './items.js'
export * from './notifications'
