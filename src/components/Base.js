import React, { Component } from 'react'

import './Base.css'

class Base extends Component {

  render() {
    return (
      <div className="bs">
        Hello base
      </div>
    )
  }
}

export default Base
