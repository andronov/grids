import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import { items } from './items'

const rootReducer = combineReducers({
  items,
  router: routerReducer
});

export default rootReducer
