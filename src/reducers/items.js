const initialCollectionState = {
  items: [],
  devices: [],
  page: 1,
  perPage: 25,
  total: 0,
};

const initialState = {
  items:
    Object.assign({}, initialCollectionState),
};

export const items = (state = initialState, action) => {


  switch (action.type) {
    case 'tt':
      return {
        ...state
      };

    default:
      return state
  }
};
