import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink, Redirect, Route, Switch, withRouter } from 'react-router-dom'

import './App.css'
import Base from "./components/Base";

const mapStateToProps = state => {
  const { env, layout, notifications, router } = state
  return {
    env,
    layout,
    notifications,
    router
  }
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
};

export class App extends Component {

  render() {
    return (
      <div className='App'>
         <Route path="/" component={Base} />
      </div>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
